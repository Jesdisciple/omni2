using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Omni
{
	public interface INode
	{
		List<INode> ToList();

		int Length { get; }

		IRule Rule { get; }

		string Decompile();
	}
}
