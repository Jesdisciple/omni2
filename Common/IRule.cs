using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Omni
{
	public interface IRule
	{
		string Name { get; }

		List<INode> Match(List<INode> nodes);
	}
}
