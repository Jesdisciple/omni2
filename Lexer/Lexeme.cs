using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Omni.Lexer
{
	public class Lexeme : INode
	{
		public readonly string symbol;

		public readonly int scanLength;

		public int Length { get; private set; } = 1;

		public IRule Rule { get; private set; }

		public Lexeme(Token token, string symbol, int scanLength)
		{
			Rule = token;
			this.scanLength = scanLength;
			this.symbol = symbol;
		}

		public List<INode> ToList()
		{
			return new List<INode> { this };
		}

		public override string ToString()
		{
			return string.Format("<Lexeme Length=\"{0}\" Symbol=\"{1}\" Rule=\"{2}\" />", scanLength, symbol, Rule.Name);
		}

		public override bool Equals(object obj)
		{
			if (obj is Lexeme)
			{
				return Equals((Lexeme)obj);
			}

			return ReferenceEquals(this, obj);
		}

		public override int GetHashCode()
		{
			return Rule.GetHashCode() + symbol.GetHashCode() + scanLength.GetHashCode();
		}

		public bool Equals(Lexeme other)
		{
			if (ReferenceEquals(this, other)) { return true; }
			if (ReferenceEquals(null, other)) { return false; }

			if (Rule != other.Rule) { return false; }
			if (scanLength != other.scanLength) { return false; }
			if (symbol != other.symbol) { return false; }

			return true;
		}

		public string Decompile()
		{
			return symbol;
		}
	}
}
