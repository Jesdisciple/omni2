using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace Omni.Lexer
{
	public class Lexer
	{
		public Dictionary<string, Token> Tokens { get; private set; } = new Dictionary<string, Token>();

		public Token this[string name]
		{
			get { return Tokens[name]; }
		}

		public List<Lexeme> Scan(string source)
		{
			List<Lexeme> result = new List<Lexeme>();

			int i = 0;
			while (i < source.Length)
			{
				foreach (var pair in Tokens)
				{
					var name = pair.Key;
					var token = pair.Value;

					Lexeme lexeme;
					if (token.Scan(source.Substring(i), out lexeme))
					{
						i = i + lexeme.scanLength;

						result.Add(lexeme);
					}
				}
			}

			return result;
		}

		private void Register(Token token)
		{
			Tokens.Add(token.Name, token);
		}

		public Token DefineToken(string name, string pattern)
		{
			var result = new Token(name, "^" + pattern);
      Register(result);
			return result;
		}

		public Token GetToken(string name)
		{
			return Tokens[name];
		}
	}
}
