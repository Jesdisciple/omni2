using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace Omni.Lexer
{
	public class Token : IRule
	{
		public readonly Regex regex;

		public string Name { get; private set; }

		internal Token(string name, Regex regex)
		{
			this.Name = name;
			this.regex = regex;
		}

		internal Token(string name, string pattern)
			: this(name, new Regex(pattern))
		{
		}

		public bool Scan(string source, out Lexeme lexeme)
		{
			Match m = regex.Match(source);
			if (m.Success)
			{
				string symbol = m.Groups[1].Value;
				lexeme = new Lexeme(this, symbol, m.Length);
				return true;
			}

			lexeme = null;
			return false;
		}

		public Lexeme Scan(string source)
		{
			Lexeme result;
			if(!Scan(source, out result))
			{
				throw new InvalidOperationException(string.Format("{0} failed parsing \"{1}\".", this, source));
			}
			return result;
		}

		public override string ToString()
		{
			return string.Format("<Token Name=\"{0}\" Pattern=\"{1}\">", Name, regex);
		}

		public override bool Equals(object obj)
		{
			if (obj is Token)
			{
				return Equals((Token)obj);
			}

			return ReferenceEquals(this, obj);
		}

		public override int GetHashCode()
		{
			return Name.GetHashCode() + regex.GetHashCode();
		}

		public bool Equals(Token other)
		{
			if (ReferenceEquals(this, other)) { return true; }
			if (ReferenceEquals(null, other)) { return false; }
			
			if (Name != other.Name) { return false; }
			if (regex != other.regex) { return false; }

			return true;
		}

		public List<INode> Match(List<INode> nodes)
		{
			if(nodes.Count == 0)
			{
				return null;
			}

			var last = nodes.Last();
			if(last.Rule == this)
			{
				return new List<INode> { last };
			}

			return null;
		}
	}
}
