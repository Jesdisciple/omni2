using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Omni.Lispish
{
	using Runtime;

	public abstract class Expression
	{
		public abstract Type Type { get; }

		public abstract Value Evaluate();
	}

	public class Expression<T> : Expression where T : Value
	{
		readonly T value;

		public override Type Type { get { return value.Type; } }

		public Expression(T value)
		{
			this.value = value;
		}

		public override Value Evaluate()
		{
			return value;
		}
	}
}
