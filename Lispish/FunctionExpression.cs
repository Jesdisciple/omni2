using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Omni.Runtime;

namespace Omni.Lispish
{
	using Runtime;

	public class FunctionExpression : Expression
	{
		readonly NativeFunction function;

		readonly Expression[] arguments;

		public override Type Type { get { return function.ReturnType; } }

		public FunctionExpression(NativeFunction function, params Expression[] arguments)
		{
			this.function = function;
			this.arguments = arguments;
		}

		public override Value Evaluate()
		{
			return function.Call(arguments.Select((e) => e.Evaluate()));
		}
	}
}
