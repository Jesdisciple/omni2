using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Omni.Lispish
{
	using Lexer;
	using Parser;
	using Runtime;

	public class Lisp
	{
		public static Lexer CreateLexer()
		{
			var lexer = new Lexer();

			lexer.DefineToken("lparen", @"\s*(\()\s*");
			lexer.DefineToken("rparen", @"\s*(\))\s*");
			lexer.DefineToken("number", @"\s*([-+]?\d+(?:\.\d+)?(?:e[-+]?\d+)?)\s*");
			lexer.DefineToken("identifier", @"\s*([^\d()\s][^()\s]*)\s*");

			return lexer;
		}

		public static Parser CreateParser()
		{
			var lexer = CreateLexer();

			var parser = new Parser(lexer);

			var lparen = parser.GetRule("lparen");
			var rparen = parser.GetRule("rparen");
			var number = parser.GetRule("number");
			var identifier = parser.GetRule("identifier");

			var list = parser.DefineRule("list", lparen.And(parser["listRest"]).And(rparen));
			var listRest = parser.DefineRule("listRest", parser["listBase"].Or(parser["listRest"].And(parser["listBase"])));
			var listBase = parser.DefineRule("listBase", list.Or(number).Or(identifier));

			return parser;
		}

		public List Compile(List logic)
		{
			logic = Runtime.List((IEnumerable<Value>)logic);

			for (int i = 0; i < logic.Count; i++)
			{
				var step = logic[i];

				if (step.Type == Runtime.ListType)
				{
					logic[i] = Compile((List)step);
				}

				if (step.Type == Runtime.IdentifierType)
				{
					Place variable;
					if (Runtime.TryResolveVariable(((Identifier)step).name, out variable))
					{
						logic[i] = variable;
					}
				}
			}

			return logic;
		}

		public Lexer Lexer { get; private set; }
		public Parser Parser { get; private set; }
		public Runtime Runtime { get; private set; }

		public Lisp(Runtime runtime, Parser parser)
		{
			Parser = parser;
			Lexer = parser.lexer;

			Runtime = runtime;
		}

		public Lisp(Runtime runtime)
			: this(runtime, CreateParser())
		{
		}

		public Lisp()
			: this(new Runtime())
		{
		}

		public List Parse(string source)
		{
			var parseTree = Parser.Parse(source);

			return BuildForm(parseTree);
		}

		public List Parse(IEnumerable<INode> lexemes)
		{
			var parseTree = Parser.Parse(lexemes);

			return BuildForm(parseTree);
		}

		public List BuildForm(INode node)
		{
			if (node.Rule == Lexer["number"])
			{
				var lexeme = (Lexeme)node;
				return Runtime.List(Runtime.Number(double.Parse(lexeme.symbol)));
			}

			if (node.Rule == Lexer["identifier"])
			{
				var lexeme = (Lexeme)node;
				return Runtime.List(Runtime.Identifier(lexeme.symbol));
			}

			if (node.Rule is Token)
			{
				return Runtime.List();
			}

			if (node.Rule == Parser["list"])
			{
				var ienum = node.ToList()
					.SelectMany(BuildForm);
				return Runtime.List(Runtime.List(ienum));
			}

			if (node.Rule == Parser["listRest"] || node.Rule == Parser["listBase"])
			{
				return Runtime.List(node.ToList()
					.SelectMany(BuildForm)
					.ToList());
			}

			return Runtime.List();
		}
	}
}
