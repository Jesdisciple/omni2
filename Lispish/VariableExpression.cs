using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Omni.Lispish
{
	using Runtime;

	public class VariableExpression : Expression
	{
		readonly Place variable;

		public override Type Type { get { return variable.placeType; } }

		public VariableExpression(Place variable)
		{
			this.variable = variable;
		}

		public override Value Evaluate()
		{
			return variable.Value;
		}
	}
}
