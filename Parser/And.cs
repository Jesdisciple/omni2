using System.Linq;
using System.Collections.Generic;

namespace Omni.Parser
{
	internal class And : RulePattern
	{
		public And(IEnumerable<IRule> children)
			: base(children)
		{
		}

		public And(params IRule[] children)
			: base(children)
		{
		}

		public override List<INode> Match(List<INode> nodes)
		{
			var result = new List<INode>();

			var cursor = nodes.Count;

			List<INode> temp;

			foreach (var rule in ((IEnumerable<IRule>)children).Reverse())
			{
				temp = rule.Match(nodes.GetRange(0, cursor));
				if(temp == null)
				{
					return null;
				}

				cursor -= temp.Count;
				result = temp.Concat(result).ToList();
			}

			return result;
		}
	}
}