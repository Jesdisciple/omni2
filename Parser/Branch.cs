using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Omni.Parser
{
	public class Branch : INode
	{
		public readonly List<INode> nodes;

		public IRule Rule { get; private set; }

		public int Length { get; private set; }

		public Branch(IRule rule, INode node)
		{
			nodes = new List<INode> { node };
			Rule = rule;
			Length = 1;
		}

		public Branch(IRule rule, IEnumerable<INode> nodes)
		{
			this.nodes = nodes.ToList();
			Rule = rule;
			Length = this.nodes.Count;
		}

		public Branch(IRule rule, params INode[] nodes)
			: this(rule, (IEnumerable<INode>)nodes)
		{
		}

		public Branch(IRule rule, List<INode> nodes)
		{
			Rule = rule;
			this.nodes = nodes;
			Length = nodes.Count;
		}

		public List<INode> ToList()
		{
			return nodes.ToList();
		}

		public string Decompile()
		{
			return string.Join(" ", nodes.Select((n) => n.Decompile()));
		}

		public override bool Equals(object obj)
		{
			if (obj is Branch)
			{
				return Equals((Branch)obj);
			}

			return ReferenceEquals(this, obj);
		}

		public override int GetHashCode()
		{
			return Rule.GetHashCode() + Length.GetHashCode() + nodes.Sum((n) => n.GetHashCode());
		}

		public bool Equals(Branch other)
		{
			if (ReferenceEquals(other, null)) { return false; }
			if (ReferenceEquals(other, this)) { return true; }
			
			if (Rule != other.Rule) { return false; }
			if (Length != other.Length) { return false; }

			var otherNodes = other.nodes;
			for (int i = 0; i < Length; i++)
			{
				if (!nodes[i].Equals(otherNodes[i]))
				{
					return false;
				}
			}

			return true;
		}

		public override string ToString()
		{
			return string.Format("<Branch Length=\"{0}\" Name=\"{1}\"><Nodes>{2}</Nodes></Branch>", Length, Rule.Name, string.Join("\n", nodes));
		}
	}
}
