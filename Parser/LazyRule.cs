using System;
using System.Collections.Generic;
using System.Diagnostics;

namespace Omni.Parser
{
	internal class LazyRule : IRule
	{
		readonly Func<IRule> inner;

		public string Name { get { return inner().Name; } }

		public LazyRule(Func<IRule> inner)
		{
			this.inner = inner;
		}

		public List<INode> Match(List<INode> nodes)
		{
			var result = inner().Match(nodes);

			return result;
		}
	}
}
