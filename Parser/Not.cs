using System;
using System.Collections.Generic;
using System.Linq;

namespace Omni.Parser
{
	internal class Not : RulePattern
	{
		public Not(IEnumerable<IRule> children)
			: base(children)
		{
			if(children.Count() != 2)
			{
				throw new ArgumentException("A \"Not\" rule-pattern must have exactly 2 operands.");
			}
		}

		public Not(IRule a, IRule b)
			: base(new List<IRule> { a, b })
		{
		}

		public override List<INode> Match(List<INode> nodes)
		{
			var result = children.First().Match(nodes);

			foreach(var rule in children.GetRange(1, children.Count - 1))
			{
				if(rule.Match(nodes) != null)
				{
					return null;
				}
			}

			return result;
		}
	}
}