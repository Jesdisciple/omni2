using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Omni.Parser
{
	internal class Or : RulePattern
	{
		public Or(IEnumerable<IRule> children)
			: base(children)
		{
		}

		public Or(params IRule[] children)
			: base(children)
		{
		}

		public override List<INode> Match(List<INode> nodes)
		{
			int childCount = children.Count;
			int nodeCount = nodes.Count;
			
			List<INode> results;

			foreach(var rule in ((IEnumerable<IRule>)children).Reverse())
			{
				results = rule.Match(nodes);
				if(results != null)
				{
					return results;
				}
			}

			return null;
		}
	}
}
