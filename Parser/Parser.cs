using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Omni.Parser
{
	using System.Diagnostics;
	using Lexer;

	public class Parser
	{
		private Dictionary<string, Rule> rules = new Dictionary<string, Rule>();

		public readonly Lexer lexer;

		public IRule this[string name]
		{
			get { return GetRule(name); }
		}

		public IRule GetRule(string name)
		{
			return GetRule_impl(name) ?? new LazyRule(() => GetRule_impl(name));
		}

		private IRule GetRule_impl(string name)
		{
			Rule rule;
			if (rules.TryGetValue(name, out rule))
			{
				return rule;
			}

			Token token;
			if( lexer.Tokens.TryGetValue(name, out token))
			{
				return token;
			}

			return null;
		}

		public Parser(Lexer lexer)
		{
			this.lexer = lexer;
		}

		public INode Parse(string source)
		{
			return Parse(lexer.Scan(source).Cast<INode>());
		}

		public INode Parse(IEnumerable<INode> lexemes)
		{
			var nodes = ParseToList(lexemes);
			Debug.Assert(nodes.Count == 1);
			return nodes[0];
		}

		public List<INode> ParseToList(string source)
		{
			return ParseToList(lexer.Scan(source).Cast<INode>());
		}

		public List<INode> ParseToList(IEnumerable<INode> lexemes)
		{
			var nodes = new List<INode>();

			foreach (var lexeme in lexemes)
			{
				nodes.Add(lexeme);

				// Replace the end until no matches are found.
				do
				{
					//Debug.WriteLine(string.Join(" ", nodes.Select((n) => n.Decompile())));
				}
				while (ParseEnd(ref nodes));
			}

			//Debug.WriteLine(string.Join("", nodes));

			return nodes;
		}

		private bool ParseEnd(ref List<INode> nodes)
		{
			foreach (var pair in rules)
			{
				var name = pair.Key;
				var rule = pair.Value;

				//Debug.WriteLine(name);

				var node = rule.Parse(nodes);
				if (node != null)
				{
					nodes.RemoveRange(nodes.Count - node.Length, node.Length);
					nodes.Add(node);
					return true;
				}
			}

			return false;
		}

		public Rule DefineRule(string name, RulePattern rule)
		{
			var result = new Rule(name, rule);
			rules.Add(name, result);
			return result;
		}
	}
}
