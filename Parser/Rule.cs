using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;

namespace Omni.Parser
{
	public class Rule : IRule
	{
		private readonly RulePattern pattern;

		public string Name { get; private set; }

		public Rule(string name, RulePattern pattern)
		{
			this.Name = name;
			this.pattern = pattern;
		}

		public List<INode> Match(List<INode> nodes)
		{
			if (nodes.Count == 0)
			{
				return null;
			}

			var last = nodes.Last();
			if (last.Rule == this)
			{
				return new List<INode> { last };
			}
			return null;
		}

		public INode Parse(List<INode> nodes)
		{
			var temp = pattern.Match(nodes);

			if (temp == null)
			{
				return null;
			}

			return new Branch(this, temp);
		}
	}
}