using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;

namespace Omni.Parser
{
	public abstract class RulePattern : IRule
	{
		protected List<IRule> children;

		public string Name { get { return "<RulePattern>"; } }

		public RulePattern(IEnumerable<IRule> children)
		{
			this.children = children.ToList();


		}

		public abstract List<INode> Match(List<INode> nodes);
	}
}