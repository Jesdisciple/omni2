using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Omni.Parser
{
	public static class Rules
	{
		public static RulePattern Or(this IRule a, IRule b)
		{
			return new Or(a, b);
			/*
			return new RulePattern((List<INode> nodes, RulePattern pattern) =>
			{
				var x = b.Match(nodes);
				if (x == null)
				{
					var y = a.Match(nodes);
					if (y == null)
					{
						return null;
					}

					return y;
				}

				return x;
			});
			*/
		}

		public static RulePattern And(this IRule a, IRule b)
		{
			return new And(a, b);

			/*
			return new RulePattern((List<INode> nodes, RulePattern pattern) =>
			{
				var y = b.Match(nodes);
				if (y == null)
				{
					return null;
				}

				nodes = nodes.GetRange(0, nodes.Count - y.Count);
				if (nodes.Count == 0)
				{
					return null;
				}

				var x = a.Match(nodes);
				if (x == null)
				{
					return null;
				}
				return x.Concat(y).ToList();
			});
			*/
		}

		public static RulePattern Not(this IRule a, IRule b)
		{
			return new Not(a, b);

			/*
			return new RulePattern((List<INode> nodes, RulePattern pattern) =>
			{
				var x = a.Match(nodes);
				if (x == null)
				{
					return null;
				}

				nodes = nodes.GetRange(nodes.Count - x.Count, x.Count);
				var y = b.Match(nodes);
				if (y == null)
				{
					return x;
				}

				return null;
			});
			*/
		}
	}
}
