﻿using System;
using System.Runtime.Serialization;

namespace Omni.Runtime
{
	[Serializable]
	internal class DuplicateNameException : Exception
	{
		public DuplicateNameException()
		{
		}

		public DuplicateNameException(string message) : base(message)
		{
		}

		public DuplicateNameException(string message, Exception innerException) : base(message, innerException)
		{
		}

		protected DuplicateNameException(SerializationInfo info, StreamingContext context) : base(info, context)
		{
		}
	}
}