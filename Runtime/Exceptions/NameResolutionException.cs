﻿using System;
using System.Runtime.Serialization;

namespace Omni.Runtime
{
	[Serializable]
	internal class NameResolutionException : Exception
	{
		public NameResolutionException()
		{
		}

		public NameResolutionException(string message) : base(message)
		{
		}

		public NameResolutionException(string message, Exception innerException) : base(message, innerException)
		{
		}

		protected NameResolutionException(SerializationInfo info, StreamingContext context) : base(info, context)
		{
		}
	}
}