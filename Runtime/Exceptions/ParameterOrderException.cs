﻿using System;
using System.Runtime.Serialization;

namespace Omni.Runtime
{
	[Serializable]
	internal class ParameterOrderException : Exception
	{
		public ParameterOrderException()
		{
		}

		public ParameterOrderException(string message) : base(message)
		{
		}

		public ParameterOrderException(string message, Exception innerException) : base(message, innerException)
		{
		}

		protected ParameterOrderException(SerializationInfo info, StreamingContext context) : base(info, context)
		{
		}
	}
}