using System;
using System.Runtime.Serialization;

namespace Omni.Runtime
{
	[Serializable]
	internal class ArgumentOrderException : ArgumentException
	{
		public ArgumentOrderException()
		{
		}

		public ArgumentOrderException(string message) : base(message)
		{
		}

		public ArgumentOrderException(string message, Exception innerException) : base(message, innerException)
		{
		}

		protected ArgumentOrderException(SerializationInfo info, StreamingContext context) : base(info, context)
		{
		}
	}
}