using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Omni.Runtime
{
	public class Function : Value, IFunction
	{
		public readonly string name;

		public Type ReturnType { get; private set; }

		List<Parameter> parameters;

		List body;

		public Function(Runtime runtime, Type returnType, IEnumerable<Parameter> parameters, List body)
			: base(runtime.FunctionType)
		{
			this.parameters = parameters.ToList();

			this.body = body;
		}

		public bool Accepts(IEnumerable<Type> arguments)
		{
			return Accepts((IEnumerable<Type>)arguments.ToList());
		}

		public bool Accepts(List<Type> argumentTypes)
		{
			if (parameters.Count != argumentTypes.Count)
			{
				return false;
			}

			for (int i = 0; i < argumentTypes.Count; i++)
			{
				if (argumentTypes[i] != parameters[i].type)
				{
					return false;
				}
			}

			return true;
		}

		public Value Call(IEnumerable<Value> arguments)
		{
			return Call((IEnumerable<Type>)arguments.ToList());
		}

		public Value Call(List<Value> arguments)
		{
			Namespace ns = new Namespace(runtime);
			for (int i = 0; i < arguments.Count; i++)
			{
				ns.DeclareVariable(parameters[i].type, parameters[i].name).Value = arguments[i];
			}

			return runtime.Execute(ns, body);
		}
	}
}
