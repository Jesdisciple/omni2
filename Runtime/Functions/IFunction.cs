using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Omni.Runtime
{
	public interface IFunction
	{
		Type ReturnType { get; }

		bool Accepts(IEnumerable<Type> arguments);
		bool Accepts(List<Type> arguments);

		Value Call(IEnumerable<Value> arguments);
		Value Call(List<Value> arguments);
	}
}
