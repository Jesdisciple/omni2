namespace Omni.Runtime
{
	public class Parameter
	{
		internal readonly Type type;

		internal readonly string name;

		public Parameter(Type type, string name)
		{
			this.type = type;
			this.name = name;
		}
	}
}
