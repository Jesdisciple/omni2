using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Omni.Runtime
{
	public class Boolean : Value
	{
		public readonly bool value;

		public Boolean(Runtime runtime, bool value)
			: base(runtime.BooleanType)
		{
			this.value = value;
		}
	}
}
