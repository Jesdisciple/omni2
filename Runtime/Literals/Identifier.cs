using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Omni.Runtime
{
	public class Identifier : Value
	{
		public readonly string name;

		internal Identifier(Runtime runtime, string name)
			: base(runtime.IdentifierType)
		{
			this.name = name;
		}

		public override bool Equals(object other)
		{
			var id = other as Identifier;
			if(id == null)
			{
				return false;
			}

			return name == id.name;
		}

		public override int GetHashCode()
		{
			return name.GetHashCode();
		}
	}
}
