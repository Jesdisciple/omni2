using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Omni.Runtime
{
	public class List : Value, IList<Value>
	{
		List<Value> items;

		public int Count { get { return items.Count; } }

		public bool IsReadOnly { get { return false; } }

		public Value this[int index]
		{
			get { return items[index]; }
			set { items[index] = value; }
		}

		internal List(Runtime runtime)
		{
			items = new List<Value>();
		}

		internal List(Runtime runtime, IEnumerable<Value> items)
			: base(runtime.ListType)
		{
			this.items = items.ToList();
		}

		public void Add(Value item)
		{
			items.Add(item);
		}

		public int IndexOf(Value item)
		{
			return items.IndexOf(item);
		}

		public void Insert(int index, Value item)
		{
			items.Insert(index, item);
		}

		public void RemoveAt(int index)
		{
			items.RemoveAt(index);
		}

		public void Clear()
		{
			items.Clear();
		}

		public bool Contains(Value item)
		{
			return items.Contains(item);
		}

		public void CopyTo(Value[] array, int arrayIndex)
		{
			items.CopyTo(array, arrayIndex);
		}

		public bool Remove(Value item)
		{
			return items.Remove(item);
		}

		public IEnumerator<Value> GetEnumerator()
		{
			return items.GetEnumerator();
		}

		IEnumerator IEnumerable.GetEnumerator()
		{
			return GetEnumerator();
		}
	}
}
