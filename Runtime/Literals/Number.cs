using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Omni.Runtime
{
	public class Number : Value
	{
		public readonly double value;

		internal Number(Runtime runtime, double value)
			: base(runtime.NumberType)
		{
			this.value = value;
		}

		public override bool Equals(object other)
		{
			var number = other as Number;
      if (number == null)
			{
				return false;
			}

			return value == number.value;
		}

		public override int GetHashCode()
		{
			return value.GetHashCode();
		}

		public Number Plus(Number other)
		{
			return runtime.Number(value + other.value);
		}

		public Number Minus(Number other)
		{
			return runtime.Number(value - other.value);
		}

		public Number Times(Number other)
		{
			return runtime.Number(value * other.value);
		}

		public Number Div(Number other)
		{
			return runtime.Number(value / other.value);
		}

		public Number Mod(Number other)
		{
			return runtime.Number(value % other.value);
		}
	}
}
