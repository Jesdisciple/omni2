using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Omni.Runtime
{
	public class String : Value
	{
		public readonly string value;

		internal String(Runtime runtime, string value)
			: base(runtime.StringType)
		{
			this.value = value;
		}

		public override bool Equals(object other)
		{
			var str = other as String;
			if (str == null)
			{
				return false;
			}

			return value == str.value;
		}

		public override int GetHashCode()
		{
			return value.GetHashCode();
		}
	}
}
