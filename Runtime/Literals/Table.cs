using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Omni.Runtime
{
	public class Table : Value, IDictionary<string, Place>
	{
		readonly SortedDictionary<string, Place> items;

		public Place this[string key]
		{
			get { return items[key]; }
			set { items[key] = value; }
		}

		public int Count { get { return items.Count; } }

		public bool IsReadOnly { get { return false; } }

		public ICollection<string> Keys { get { return items.Keys; } }

		public ICollection<Place> Values { get { return items.Values; } }

		public Place this[String key]
		{
			get { return this[key.value]; }
			set { this[key.value] = value; }
		}

		public Table(Runtime runtime)
		 : base(runtime.TableType)
		{
			items = new SortedDictionary<string, Place>();
		}

		public Table(Runtime runtime, List items)
			: this(runtime)
		{
			List list;

			Type type;

			Value key, value;

			Place place;

			Type listType = runtime.ListType;
			Type typeType = runtime.TypeType;
			Type stringType = runtime.StringType;

			foreach (var triple in items)
			{
				if (triple.Type != listType) { runtime.Throw("Invalid list passed to table constructor."); }

				list = (List)triple;

				if (list.Count != 3) { runtime.Throw("Invalid list passed to table constructor: items[{0}].Count == {1}"); }

				if (!list[0].TryConvertTo(typeType, out type))
				{
					runtime.Throw("Invalid list passed to table constructor.");
				}
				key = list[0];
				value = list[1];
				place = new Place(runtime, value.Type, value);

				if (key.Type != stringType) { runtime.Throw("Invalid list passed to table constructor."); }

				Add(((String)key).value, place);
			}
		}

		public void Add(KeyValuePair<string, Place> item)
		{
			((IDictionary<string, Place>)items).Add(item);
		}

		public void Add(string key, Place value)
		{
			items.Add(key, value);
		}

		public void Clear()
		{
			items.Clear();
		}

		public bool Contains(KeyValuePair<string, Place> item)
		{
			return items.Contains(item);
		}

		public bool ContainsKey(string key)
		{
			return items.ContainsKey(key);
		}

		public void CopyTo(KeyValuePair<string, Place>[] array, int arrayIndex)
		{
			CopyTo(array, arrayIndex);
		}

		public bool Remove(KeyValuePair<string, Place> item)
		{
			return ((IDictionary<string, Place>)items).Remove(item);
		}

		public bool Remove(string key)
		{
			return items.Remove(key);
		}

		public bool TryGetValue(string key, out Place value)
		{
			return items.TryGetValue(key, out value);
		}

		public IEnumerator<KeyValuePair<string, Place>> GetEnumerator()
		{
			return items.GetEnumerator();
		}

		IEnumerator IEnumerable.GetEnumerator()
		{
			return GetEnumerator();
		}

		public override bool TryConvertTo<T>(Type type, out T value)
		{
			throw new NotImplementedException();
		}
	}
}
