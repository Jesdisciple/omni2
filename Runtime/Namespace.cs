using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Omni.Runtime
{
	public class Namespace
	{
		Table variables;

		Runtime runtime;

		internal Place this[string name]
		{
			get { return variables[name]; }
			set { variables[name] = value; }
		}

		public Namespace(Runtime runtime)
		{
			this.runtime = runtime;

			variables = new Table(runtime);
		}

		public bool TryDeclareVariable(Type type, string name, out Place variable)
		{
			if (variables.ContainsKey(name))
			{
				variable = null;
				return false;
			}

			variable = new Place(runtime, type);
			variables[name] = variable;
			return true;
		}

		public bool TryResolveVariable(string name, out Place variable)
		{
			if(variables.TryGetValue(name, out variable))
			{
				return true;
			}

			variable = null;
			return false;
		}

		public Place DeclareVariable(Type type, string name)
		{
			Place result;
			if (TryDeclareVariable(type, name, out result))
			{
				return result;
			}

			throw new DuplicateNameException(string.Format(
				"Cannot declare variable `{0}` in this namespace; that name is already used.",
				name));
		}

		public Place ResolveVariable(string name)
		{
			Place result;
			if(TryResolveVariable(name, out result))
			{
				return result;
			}

			throw new NameResolutionException(string.Format(
				"No variable named `{0}` exists in this namespace.",
				name));
		}
	}
}
