using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Omni.Runtime
{
	public class Place : Value
	{
		public readonly Type placeType;
		Value value;
		public Value Value
		{
			get { return value; }
			set
			{
				if (!TryAssign(value))
				{
					runtime.Throw("Attempted to assign a value to an incompatible place.");
				}
			}
		}

		internal Place(Runtime runtime, Type type, Value value = null)
			: base(runtime.PlaceType)
		{
			this.placeType = type;
			this.Value = value;
		}

		public override bool TryConvertTo<T>(Type type, out T result)
		{
			return Value.TryConvertTo(type, out result);
		}

		public bool TryAssign(Value value)
		{
			if (value != null && !value.Satisfies(placeType))
			{
				return false;
			}

			this.value = value;
			return true;
		}
	}
}
