using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Omni.Runtime
{
	public class Runtime
	{
		public Type TypeType { get; private set; }
		public Type ValueType { get; private set; }
		public Type PlaceType { get; private set; }
		public Type IdentifierType { get; private set; }
		public Type BooleanType { get; private set; }
		public Type NumberType { get; private set; }
		public Type StringType { get; private set; }

		public Type ListType { get; private set; }
		public Type TableType { get; private set; }
		public Type FunctionType { get; private set; }

		List<Namespace> stack;

		IEnumerable<Namespace> Scopes
		{
			get
			{
				return ((IEnumerable<Namespace>)stack).Reverse();
			}
		}

		Namespace CurrentScope
		{
			get { return Scopes.First(); }
		}

		public Runtime()
		{
			TypeType = new Type(this);
			ValueType = Type("value");
			PlaceType = Type("place");
			BooleanType = Type("boolean");
			NumberType = Type("number");
			StringType = Type("string");
			ListType = Type("list");
			TableType = Type("table");
			FunctionType = Type("function");
			IdentifierType = Type("identifier");

			stack = new List<Namespace> { new Namespace(this) };

			DefineType(TypeType);
			DefineType(ValueType);
			DefineType(PlaceType);
			DefineType(BooleanType);
			DefineType(NumberType);
			DefineType(StringType);
			DefineType(ListType);
			DefineType(TableType);
			DefineType(FunctionType);
			DefineType(IdentifierType);
		}

		private void DefineType(Type type)
		{
			DeclareVariable(TypeType, type.name).Value = type;
		}

		public bool TryDeclareVariable(Type type, string name, out Place variable)
		{
			return CurrentScope.TryDeclareVariable(type, name, out variable);
		}

		public void PushNamespace(Namespace ns)
		{
			stack.Add(ns);
		}

		public void PopNamespace()
		{
			stack.RemoveAt(stack.Count - 1);
		}

		public Value Execute(List body)
		{
			return Execute(new Namespace(this), body);
		}

		public Value Execute(Namespace ns, List body)
		{
			PushNamespace(ns);

			for (int i = 0; i < body.Count; i++)
			{
				var step = body[i];

				if (step.Type == ListType)
				{
					body[i] = Call((List)step);
				}
			}

			PopNamespace();

			return body.Last();
		}

		public Value Execute(Namespace ns, Func<Runtime, Value> f)
		{
			PushNamespace(ns);

			var result = f(this);

			PopNamespace();

			return result;
		}

		public Value Call(List list)
		{
			for (int i = 0; i < list.Count; i++)
			{
				var step = list[i];

				if (step.Type == ListType)
				{
					list[i] = Call((List)step);
				}
			}
			
			var first = list[0];
			if(first.Type == PlaceType)
			{
				first = ((Place)first).Value;
			}

			if (first.Type != FunctionType)
			{
				throw new InvalidOperationException("This value is not a function and cannot be called.");
			}

			var function = (IFunction)first;

			return function.Call(list.Skip(1));
		}

		public Place DeclareVariable(Type type, string name)
		{
			Place result;
			if (TryDeclareVariable(type, name, out result))
			{
				return result;
			}

			throw new DuplicateNameException(string.Format(
				"Cannot declare variable `{0}` in this scope; that name is already used.",
				name));
		}

		public bool TryResolveVariable(string name, out Place variable)
		{
			foreach (var ns in Scopes)
			{
				if (ns.TryResolveVariable(name, out variable))
				{
					return true;
				}
			}
			variable = null;
			return false;
		}

		public Place ResolveVariable(string name)
		{
			Place result;
			if (TryResolveVariable(name, out result))
			{
				return result;
			}

			throw new NameResolutionException(string.Format(
				"No variable named `{0}` exists in this scope or any containing scope.",
				name));
		}

		public Type Type(string name) { return new Type(this, name); }
		public Identifier Identifier(string name) { return new Identifier(this, name); }
		public Boolean Boolean(bool value) { return new Boolean(this, value); }
		public Number Number(double value) { return new Number(this, value); }
		public String String(string s) { return new String(this, s); }
		public List List(params Value[] items) { return List((IEnumerable<Value>)items); }
		public List List(IEnumerable<Value> items) { return new List(this, items); }
		public List List(params List[] items) { return List((IEnumerable<Value>)items); }
		public List List() { return new List(this); }
		public Function Function(Type returnType, IEnumerable<Parameter> parameters, List body)
		{
			return new Function(this, returnType, parameters, body);
		}
		public Function Function(Type returnType, string name, IEnumerable<Parameter> parameters, List body)
		{
			var result = Function(returnType, parameters, body);
      DeclareVariable(FunctionType, name).Value = result;
			return result;
		}
		public NativeFunction Function(Type returnType, IEnumerable<Parameter> parameters, Func<Runtime, Value> body)
		{
			return new NativeFunction(this, returnType, parameters, body);
		}
		public NativeFunction Function(Type returnType, string name, IEnumerable<Parameter> parameters, Func<Runtime, Value> body)
		{
			var result = Function(returnType, parameters, body);
			DeclareVariable(FunctionType, name).Value = result;
			return result;
		}

		public Table Table(List items = null)
		{
			if (items == null)
			{
				return new Table(this);
			}

			return new Table(this, items);
		}

		public void Throw(string message, params string[] args)
		{
			Throw(
				String(
					string.Format(
						message,
						args)));
		}

		public void Throw(Value message)
		{
			throw new NotImplementedException();
		}
	}
}
