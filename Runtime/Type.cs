using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Omni.Runtime
{
	public class Type : Value, IFunction
	{
		public readonly string name;

		public Type ReturnType { get { return runtime.PlaceType; } }

		internal Type(Runtime runtime)
		{
			name = "type";

			this.runtime = runtime;
			Type = this;
		}

		public bool IsAssignableTo(Type other)
		{
			if (other == this) { return true; }

			if (other == Type)
			{
				// The other type is our parent type.
				return true;
			}

			if (other.Type == Type)
			{
				// The other type is not this type, but it derives from the same type (i.e., it's not an ancestor).
				return false;
			}

			if (other == runtime.ValueType)
			{
				// All values are... values...
				// The value type isn't actually in the type heirarchy, hence this special case.
				return true;
			}

			if (Type == runtime.TypeType)
			{
				// The other type is not the type type or the value type, and those are the only types we can satisfy.
				return false;
			}

			// Eventually, this should run into one of the above cases.
			return Type.IsAssignableTo(other);
		}

		public bool Accepts(IEnumerable<Type> arguments)
		{
			return Accepts(arguments.ToList());
		}

		public bool Accepts(List<Type> arguments)
		{
			if(arguments.Count == 2) { return false; }
			if(arguments[0] != runtime.TypeType) { return false; }
			if(arguments[1] != runtime.IdentifierType) { return false; }
			return true;
		}

		public Value Call(IEnumerable<Value> arguments)
		{
			return Call(arguments.ToList());
		}

		public Value Call(List<Value> arguments)
		{
			var type = (Type)arguments[0];
			var id = (Identifier)arguments[1];
			var value = arguments.Count > 2 ? arguments[2] : null;

			var place = runtime.DeclareVariable(type, id.name);
      place.Value = value;
			return place;
		}

		public Type(Runtime runtime, string name)
			: base(runtime.TypeType)
		{
			this.name = name;
    }
	}
}
