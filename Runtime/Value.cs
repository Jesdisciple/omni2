using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Omni.Runtime
{
	public abstract class Value
	{
		Type type;
		public Type Type { get; internal set; }

		internal Runtime runtime;
		protected Runtime Runtime { get { return runtime; } }

		internal Value() { }

		public Value(Type type)
		{
			Type = type;
			runtime = type.runtime;
		}

		public virtual bool Satisfies(Type other)
		{
			return Type.IsAssignableTo(other);
		}

		public virtual bool TryConvertTo<T>(Type type, out T value) where T : Value
		{
			if (Satisfies(type))
			{
				value = (T)this;
				return true;
			}

			value = null;
			return false;
		}
	}
}
