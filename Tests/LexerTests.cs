using System.Collections.Generic;

using NUnit.Framework;

namespace Omni.Tests
{
	using Lexer;
	using Lispish;

	[TestFixture]
	public class LexerTests
	{
		[TestCase]
		public void ScanToken()
		{
			var lexer = Lisp.CreateLexer();

			var token = lexer.GetToken("lparen");

			Lexeme lexeme;
			Assert.IsTrue(token.Scan(" ( ", out lexeme));
			Assert.AreEqual(lexeme.scanLength, 3);
			Assert.AreEqual(lexeme.symbol, "(");
			Assert.AreEqual(lexeme, token.Scan(" ( "));
		}

		[TestCase]
		public void ScanSimpleLisp()
		{
			var lexer = Lisp.CreateLexer();

			var lexemes = lexer.Scan("(+ (+ 4 3) (+ 2 1))");

			var lparenToken = lexer.GetToken("lparen");
			var rparenToken = lexer.GetToken("rparen");
			var numberToken = lexer.GetToken("number");
			var identifierToken = lexer.GetToken("identifier");

			var standard = new List<Lexeme>
			{
				lparenToken.Scan("("),
				identifierToken.Scan("+ "),
				lparenToken.Scan("("), identifierToken.Scan("+ "), numberToken.Scan("4 "), numberToken.Scan("3"), rparenToken.Scan(") "),
				lparenToken.Scan("("), identifierToken.Scan("+ "), numberToken.Scan("2 "), numberToken.Scan("1"), rparenToken.Scan(")"),
				rparenToken.Scan(")"),
			};

			Assert.AreEqual(lexemes.Count, standard.Count);

			for (int i = 0; i < lexemes.Count; i++)
			{
				Assert.AreEqual(lexemes[i], standard[i]);
			}
		}
	}
}
