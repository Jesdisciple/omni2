using System.Collections.Generic;
using System.Linq;

using NUnit.Framework;

namespace Omni.Tests
{
	using Runtime;
	using Lispish;

	[TestFixture]
	public class LispishTests
	{
		[TestCase]
		public void ParseLispish()
		{
			var lisp = new Lisp();

			var form = lisp.BuildForm(LispishUtil.CreateParseTree(lisp.Parser));

			var standard = LispishUtil.CreateForm(lisp.Runtime);

			Assert.AreEqual(form, standard);
		}

		[TestCase]
		public void CompileLispish()
		{
			var lisp = LispishUtil.CreateLisp();

			var result = lisp.Compile(LispishUtil.CreateForm(lisp.Runtime));

			var standard = LispishUtil.CreateExecutable(lisp.Runtime);

			Assert.AreEqual(result, standard);
		}

		[TestCase]
		public void ExecuteLispish()
		{
			var lisp = LispishUtil.CreateLisp();

			var result = lisp.Runtime.Execute(LispishUtil.CreateExecutable(lisp.Runtime));

			var standard = lisp.Runtime.Number(10);

			Assert.AreEqual(result, standard);
		}
	}
}
