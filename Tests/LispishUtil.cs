using System.Collections.Generic;
using System.Linq;

namespace Omni.Tests
{
	using Parser;
	using Runtime;
	using Lispish;
	using System;

	public static class LispishUtil
	{

		public static string CreateSource()
		{
			// Whitespace matters. Parentheses must have no whitespace around them,
			// in order for the equality unit tests to work. Other tokens must have
			// whitespace which corresponds to that used in the ParseTree method.
			//     "(+ (+ 4 3)(+ 2 1))"
			return "(+ (+ 4 3)(+ 2 1))";
		}

		public static INode CreateParseTree()
		{
			return CreateParseTree(Lisp.CreateParser());
		}

		public static INode CreateParseTree(Parser parser)
		{
			var lexer = parser.lexer;

			return
				new Branch(parser["listRest"],
					new Branch(parser["listBase"],
						list(parser,
							lexer["identifier"].Scan("+ "),
							list(parser,
								lexer["identifier"].Scan("+ "),
								lexer["number"].Scan("4 "),
								lexer["number"].Scan("3")),
							list(parser,
								lexer["identifier"].Scan("+ "),
								lexer["number"].Scan("2 "),
								lexer["number"].Scan("1")))));
		}

		public static List CreateExecutable(Runtime runtime)
		{
			return
				runtime.List(
					runtime.List(
						runtime.ResolveVariable("+"),
						runtime.List(
							runtime.ResolveVariable("+"),
							runtime.Number(4),
							runtime.Number(3)),
						runtime.List(
							runtime.ResolveVariable("+"),
							runtime.Number(2),
							runtime.Number(1))));
		}

		public static List CreateForm(Runtime runtime)
		{
			return
				runtime.List(
					runtime.List(
						runtime.Identifier("+"),
						runtime.List(
							runtime.Identifier("+"),
							runtime.Number(4),
							runtime.Number(3)),
						runtime.List(
							runtime.Identifier("+"),
							runtime.Number(2),
							runtime.Number(1))));
		}

		public static Lisp CreateLisp()
		{
			var lisp = new Lisp();

			var runtime = lisp.Runtime;

			runtime.Function(runtime.NumberType, "+",
				new Parameter[]
				{
					new Parameter(runtime.NumberType, "a"),
					new Parameter(runtime.NumberType, "b")
				},
				(Runtime r) =>
				{
					return ((Number)r.ResolveVariable("a").Value)
						.Plus((Number)r.ResolveVariable("b").Value);
				});

			return lisp;
		}

		private static Branch list(Parser parser, params INode[] nodes)
		{
			return new Branch(parser["list"], new List<INode>
			{
				parser.lexer["lparen"].Scan("("),
				listRest(parser, nodes.ToList()),
				parser.lexer["rparen"].Scan(")")
			});
		}

		private static Branch listRest(Parser parser, IEnumerable<INode> nodes)
		{
			var list = nodes.ToList();
			var result = new List<INode> { new Branch(parser["listBase"], list.Last()) };
			if (list.Count > 1)
			{
				result.Insert(0, listRest(parser, list.GetRange(0, list.Count - 1)));
			}
			return new Branch(parser["listRest"], result);
		}
	}
}
