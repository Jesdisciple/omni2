using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using NUnit.Framework;

namespace Omni.Tests
{
	using Lexer;
	using Parser;
	using Lispish;

	[TestFixture]
	public class ParserTests
	{
		[TestCase]
		public void ParseStars()
		{
			var lexer = new Lexer();
			var asteriskToken = lexer.DefineToken("asterisk", @"(\*)");

			var parser = new Parser(lexer);
			var pairRule = parser.DefineRule("pair", asteriskToken.And(asteriskToken));
			var starsRule = parser.DefineRule("stars", parser.GetRule("starsRest").And(pairRule));
			var starsRestRule = parser.DefineRule("starsRest", pairRule.Or(parser.GetRule("stars").And(pairRule)));

			var lexemes = new List<Lexeme>
			{
				asteriskToken.Scan("*"), asteriskToken.Scan("*"),
				asteriskToken.Scan("*"), asteriskToken.Scan("*"),
				asteriskToken.Scan("*"), asteriskToken.Scan("*"),
				asteriskToken.Scan("*"), asteriskToken.Scan("*"),
			};

			var nodes = parser.ParseToList(lexemes);
			Assert.AreEqual(nodes.Count, 1);
		}

		[TestCase]
		public void ParseSimpleLisp()
		{
			Parser parser = Lisp.CreateParser();

			var node = parser.Parse(LispishUtil.CreateSource());

			var standard = LispishUtil.CreateParseTree(parser);

			Assert.AreEqual(node, standard);
		}
	}
}
